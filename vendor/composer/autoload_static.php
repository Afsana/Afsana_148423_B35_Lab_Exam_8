<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit416869dfc5c32c134e8b04cdec427025
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP148423',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit416869dfc5c32c134e8b04cdec427025::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit416869dfc5c32c134e8b04cdec427025::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
